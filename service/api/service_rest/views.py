from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.http import require_http_methods
import json

from common.json import ModelEncoder
from .models import AutomobileVO, Technician, Appointment
from .encoders import AppointmentDetailEncoder, AutomobileVOEncoder, TechnicianListEncoder


# Create your views here.

@require_http_methods(["GET", "POST"])
def api_technician(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians}, encoder=TechnicianListEncoder)

    else:
        content = json.loads(request.body)
        technician = Technician.objects.create(**content)
        return JsonResponse(
            technician,
            encoder=TechnicianListEncoder,
            safe=False,
        )



@require_http_methods(["GET", "PUT", "DELETE"])
def api_detail_technician(request, id):
    if request.method == "GET":
        technicians = Technician.objects.get(id=id)
        return JsonResponse(
            {"technicians": technicians}, encoder=TechnicianListEncoder)

    elif request.method == "PUT":
        content = json.loads(request.body)
        try:

            technician = Technician.objects.get(employee_id=content["technician"])
            technician.first_name = content["first_name"]
            technician.last_name = content["last_name"]
            return JsonResponse(technician, encoder=TechnicianListEncoder, safe=False)
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Technician doesn't exist"}, status=404)

        Technician.objects.filter(id=id).update(**content)
        technician = Technician.objects.get(id-id)
        return JsonResponse(technician, encoder=TechnicianListEncoder, safe=False)

    else:
        if request.method == "DELETE":
            count, _ = Technician.objects.filter(id=id).delete()
            return JsonResponse(
                {"deleted": count > 0})




@require_http_methods(["GET", "POST"])
def api_appointments(request):
    if request.method == "GET":
        appointments = Appointment.objects.all()
        return JsonResponse(
            {"appointments": appointments}, encoder=AppointmentDetailEncoder, safe=False)

    else:
        content = json.loads(request.body)
        technician_id = content["technician"]
        try:
            technician = Technician.objects.get(employee_id=technician_id)
            content["technician"] = technician
        except Technician.DoesNotExist:
            return JsonResponse(
                {"message": "Technician doesn't exist"}, status=404)

        appointment = Appointment.objects.create(**content)
        return JsonResponse(appointment, encoder=AppointmentDetailEncoder, safe=False)



@require_http_methods(["GET", "DELETE"])
def api_appt_detail(request, id):
    if request.method == "GET":
            appointment = Appointment.objects.get(id=id)
            return JsonResponse(appointment, encoder=AppointmentDetailEncoder, safe=False)

    else:
        try:
            count, _ = Appointment.objects.filter(id=id).delete()
            return JsonResponse({"deleted": count > 0})
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Does not exist"}, status=404)


@require_http_methods(["PUT"])
def api_status_cancel(request, id):
    try:
        appointment = Appointment.objects.get(id=id)
        appointment.status = Appointment.Status.CANCELED
        appointment.save()
        return JsonResponse(appointment, encoder=AppointmentDetailEncoder, safe=False)

    except Appointment.DoesNotExist:
        return JsonResponse({"message": "Appointment doesn't exist"}, status=404)



@require_http_methods(["PUT"])
def api_status_finish(request, id):
    try:
        appointment = Appointment.objects.get(id=id)
        appointment.status = Appointment.Status.FINISHED
        appointment.save()
        return JsonResponse(appointment, encoder=AppointmentDetailEncoder, safe=False)

    except Appointment.DoesNotExist:
        return JsonResponse({"message": "Appointment doesn't exist"}, status=400)

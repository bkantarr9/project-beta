import json
from common.json import ModelEncoder
from .models import Appointment, AutomobileVO, Technician


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "import_href",
        "vin",
        "sold",
    ]

class TechnicianListEncoder(ModelEncoder):
    model = Technician
    properties = [
        "first_name",
        "last_name",
        "employee_id",
        "id",
    ]

class AppointmentDetailEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "customer",
        "vin",
        "technician",
        "reason",
        "date_time",
        "status",
    ]
    encoders = {"technician": TechnicianListEncoder()}

import { NavLink } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
          <div>
              <button className="btn btn-dark dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
              Inventory
              </button>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/manufacturer">List of Manufacturers</NavLink></li>
                <li><NavLink className="dropdown-item" to="/manufacturers/create">Create a Manufacturer</NavLink></li>
                <li><NavLink className="dropdown-item" to="/models">List of Models</NavLink></li>
                <li><NavLink className="dropdown-item" to="/models/create">Create a Vehicle Model</NavLink></li>
                <li><NavLink className="dropdown-item" to="/automobiles">List of Automobiles</NavLink></li>
                <li><NavLink className="dropdown-item" to="/automobiles/create">Create an Automobile</NavLink></li>
              </ul>
            </div>
            <div>
              <button className="btn btn-dark dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
              Services
              </button>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/appointments/create">Add an Appointment</NavLink></li>
                <li><NavLink className="dropdown-item" to="/appointments">Service Appointment List</NavLink></li>
                <li><NavLink className="dropdown-item" to="/technicians/create">Add a technician</NavLink></li>
                <li><NavLink className="dropdown-item" to="/technicians">Technician List</NavLink></li>
                <li><NavLink className="dropdown-item" to="/appointments/history">Service History</NavLink></li>
              </ul>
            </div>
            <div>
              <button className="btn btn-dark dropdown-toggle" type="button" data-bs-toggle="dropdown" aria-expanded="false">
              Sales
              </button>
              <ul className="dropdown-menu">
                <li><NavLink className="dropdown-item" to="/salespeople/create">Add a Salesperson</NavLink></li>
                <li><NavLink className="dropdown-item" to="/salespeople">List of Salespeople</NavLink></li>
                <li><NavLink className="dropdown-item" to="/customers/create">Add a Customer</NavLink></li>
                <li><NavLink className="dropdown-item" to="/customers">List of Customers</NavLink></li>
                <li><NavLink className="dropdown-item" to="/sales">List of Sales</NavLink></li>
                <li><NavLink className="dropdown-item" to="/sales/create">Record a new Sale</NavLink></li>
                <li><NavLink className="dropdown-item" to="/sales/history">Salesperson History</NavLink></li>
              </ul>
            </div>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;

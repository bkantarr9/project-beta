import { useEffect, useState } from "react";

function ServiceHistory() {
	const [appointments, setAppointments] = useState([]);
	const [automobiles, setAutomobiles] = useState([]);
	const [vin, setVin] = useState("");


    const fetchAppointments = async() => {
        const response = await fetch('http://localhost:8080/api/appointments/')

        if (response.ok) {
            const data = await response.json();
            setAppointments(data.appointments)
        }
    };
    useEffect(() => {
        fetchAppointments();
    }, []);

    const fetchAutomobiles = async() => {
        const anotherResponse = await fetch('http://localhost:8100/api/automobiles/')
        if (anotherResponse.ok) {
            const data = await anotherResponse.json();
            setAutomobiles(data.autos);
        };
    };
    useEffect(() => {
        fetchAutomobiles();
    }, []);


	const handleVinChange = async (e) => {
		setVin(e.target.value);
	};

	function filterAppts() {
		const filtered = appointments.filter(
			(appointment) => appointment.vin === vin,
		);
		setAppointments(filtered);
	}

	function soldVehicle(vin) {
		for (let auto of automobiles) {
			if (vin === auto["vin"]) {
				return "Yes";
			}
		}
		return "No";
	}

	function formatDate() {
		const date = new Date();
		const timeString = date.toLocaleTimeString(undefined, {
			year: "numeric",
			month: "numeric",
			day: "numeric",
			hour: "numeric",
			minute: "2-digit",
		});
		return timeString.replace(/\s/g, "").replace(",", ", ");
	}

	return (
		<>
			<h1>Service History</h1>
			<div className="form-floating mb-3">
				<input
					value={vin}
					onChange={handleVinChange}
					placeholder="Vin"
					required
					type="text"
					name="vin"
					id="vin"
					className="form-control"
				/>
				<label htmlFor="vin">Search by VIN...</label>{" "}
				<button
					onClick={() => filterAppts()}
					type="button"
					className="btn btn-primary"
				>
					Search
				</button>
			</div>
			<table className="table table-striped">
				<thead>
					<tr>
						<th>VIN</th>
						<th>Is VIP?</th>
						<th>Customer name</th>
						<th>Date and Time</th>
						<th>Technician</th>
						<th>Reason</th>
						<th>Status</th>
					</tr>
				</thead>
				<tbody>
					{appointments.map((appointment) => {
						return (
							<tr key={appointment.id}>
								<td>{appointment.vin}</td>
								<td>{soldVehicle(appointment.vin)}</td>
								<td> {appointment.customer}</td>
								<td> {formatDate(appointment.date_time)}</td>
								<td>
									{`${appointment.technician.first_name} ${appointment.technician.last_name}`}
								</td>
								<td> {appointment.reason}</td>
								<td> {appointment.status}</td>
							</tr>
						);
					})}
				</tbody>
			</table>
		</>
	);
}

export default ServiceHistory;

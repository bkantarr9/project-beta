from django.db import models
from django.urls import reverse
# Classes 
# > Salesperson (first_name, last_name and employee_id)
# > Customer (first_name, last_name, address and phone number)
# > Sale (price is not foreign , -automobile, salesperson and customer- foreign key)
# > AutomobileVO (vin and sold)

class Salesperson(models.Model):
    first_name = models.CharField(max_length = 100)
    last_name = models.CharField(max_length = 100)
    employee_id = models.CharField(max_length = 200, unique=True)


    def __str__(self):
        return f"{self.first_name} {self.last_name}"
    
    def get_api_url(self):
        return reverse("api_list_salespeople", kwargs={"id": self.id})
    
   

class AutomobileVO(models.Model):
    vin = models.CharField(max_length=50, unique=True)
    sold = models.BooleanField(default=False)
    import_href = models.CharField(max_length=200, unique=True, null=True)

    def __str__(self):
        return self.vin
    
    # def get_api_url(self):
    #     return reverse("api_list_automobilevo", kwargs={"id": self.id})



class Customer(models.Model):
    first_name = models.CharField(max_length = 100)
    last_name = models.CharField(max_length = 100)
    address = models.CharField(max_length = 200)
    phone_number = models.CharField(max_length = 15, unique=True)


    def __str__(self):
        return f"{self.first_name} {self.last_name}"
    
    def get_api_url(self):
        return reverse("api_list_customers", kwargs={"id": self.id})



class Sale(models.Model):
    price = models.PositiveIntegerField()
    automobile = models.ForeignKey(
        AutomobileVO,
        related_name = "automobile",
        on_delete = models.CASCADE,
    )
    salesperson = models.ForeignKey(
        Salesperson,
        related_name = "salesperson",
        on_delete = models.PROTECT,
    )
    customer = models.ForeignKey(
        Customer,
        related_name = "customer",
        on_delete = models.PROTECT,
    )

    def get_api_url(self):
        return reverse("api_list_sales", kwargs={"id": self.id})




  


   


    


